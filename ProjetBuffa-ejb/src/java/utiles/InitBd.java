/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utiles;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.ejb.Startup;
import session.GestionnairePersonnes;
import session.GestionnaireTaches;

/**
 *
 * @author decha
 */
@Singleton
@LocalBean
@Startup
public class InitBd {
@EJB
GestionnairePersonnes gp;
@EJB
GestionnaireTaches gt;

    public InitBd() {
        System.out.println("---InitBd");
        
    }
    @PostConstruct
    public void remplisBdtestdata(){
        gp.creerPersonneTest();
        System.err.println("@@j'init la bd");
        gt.creerTacheTest();
    }
    
   
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
